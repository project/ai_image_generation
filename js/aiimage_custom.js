(function ($, Drupal) {
  Drupal.behaviors.checkboxToggle = {
    attach: function (context, settings) {
      function checkCheckboxes() {
        var anyChecked = $('.aibox input[type="checkbox"]:checked').length > 0;
          if(anyChecked){
            $('#aiimagesavebtn').attr("disabled", false);
            $('#aiimagesavebtn').removeClass("is-disabled");
          }
          else{
            $('#aiimagesavebtn').attr("disabled", true);
            $('#aiimagesavebtn').addClass("is-disabled");
          }
      }

      $('[id^=save-image-checkbox-]').change(function() {
        var checkboxId=  $(this).attr('id');
        var number = checkboxId.replace('save-image-checkbox-', '');
        var hiddenchkboxid = '#AIimage-checkbox-' + number;
        if ($(this).is(':checked')) {
          $(hiddenchkboxid).prop('checked', true).trigger('change');
        } else {
          $(hiddenchkboxid).prop('checked', false).trigger('change');
        }
        checkCheckboxes();
      });


      // model selection+

      $('#ai-model').on('change', function () {
        // Get the selected value.
        var x =2;
        var selectedValue =$(this).val();
        if (selectedValue=='dall-e-3'){
          $("#d3image-parameter-wrapper").removeClass('hide-this');
          $("#dalle-imgsize  option[value='256x256']").addClass('hide-this');
          $("#dalle-imgsize  option[value='512x512']").addClass('hide-this');
          $("#dalle-imgsize  option[value='1024x1792']").removeClass('hide-this');
          $("#dalle-imgsize  option[value='1792x1024']").removeClass('hide-this');
          $("#dalle-imgsize  option[value='1024x1024']").prop("selected", true);
           console.log("executed");
           var selectobject = document.getElementById("aitotalimage");
           for (var i=2; i<=selectobject.length; i++) {
                 $("#aitotalimage  option[value='" + i + "']").addClass('hide-this');
              }
              document.getElementById('aitotalimage').value = "1";

        }
        else if (selectedValue=='dall-e-2'){
          $("#dalle-imgsize  option[value='256x256']").removeClass('hide-this');
          $("#dalle-imgsize  option[value='512x512']").removeClass('hide-this');
          $("#dalle-imgsize  option[value='1024x1792']").addClass('hide-this');
          $("#dalle-imgsize  option[value='1792x1024']").addClass('hide-this');
          $("#d3image-parameter-wrapper").addClass('hide-this');
          var selectobject = document.getElementById("aitotalimage");
          for (var i=2; i<=selectobject.length; i++) {
                  $("#aitotalimage  option[value='" + i + "']").removeClass('hide-this');
              }
        }


      });


    }
  };
})(jQuery, Drupal);
