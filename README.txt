 
# AI Images Generation

## Description
This module  generate text to image using OPEN AI plateform.

## Dependencies
 -https://www.drupal.org/project/media

## Requirements
- Drupal 9,10

## Installation
- Ensure all dependencies are installed.
- Enable the module.
- Generate APIkey from open AI plateform and add API key here in (/admin/content/ai_image_generation_settings)
- To generate API keys login to https://platform.openai.com   

## Usage
- Go to (/admin/content/ai_image_generation), enter the prompt for image generation  and other paramneter and click generate image button.
- Image will be generated as per the description given.
- You can also save the image in media library byselecting image and clicking save button.

## How to use
- Navigate to the node add/edit page.
- In media field select the image you saved using AI.
- OR  Click on AI Image icon to generate a new image.

## Maintainers
- [Maintainer Samata Soni](https://www.drupal.org/u/samata-soni)
