<?php
namespace Drupal\ai_image_generation\Controller;

use Drupal\Core\Controller\ControllerBase;


class APIUsage extends ControllerBase {
  /**
   * Returns a renderable array for a table with dummy data.
   */
  public function usagePage() {
    $header = [
      $this->t('<h3>Model</h3>'),
      $this->t('</h3>Service</h3>'),
      $this->t('</h3>Resolution</h3>'),
      $this->t('</h3>Price</h3>'),
    ];

    $rows = [
      [$this->t('<b>DALL·E 3</b>'),'Standard' ,'1024x1024', '$0.040 / image'],
      [$this->t('<b>DALL·E 3</b>') ,'Standard', '1024x1792, 1792x1024', '$0.080 / image'],
      [$this->t('<b>DALL·E 3</b>'),'HD', '1024x1024', '$0.080 / image'],
      [$this->t('<b>DALL·E 3</b>'),'HD', '1024x1792, 1792x1024', '$0.120 / image'],
      [$this->t('<b>DALL·E 2</b>'),'  -', '1024x1024', '$0.020 / image'],
      [$this->t('<b>DALL·E 2</b>'),'  -',  '512x512', '$0.018 / image'],
      [$this->t('<b>DALL·E 2</b>'),'  -',  '256x256', '$0.016 / image'],
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => ['dalle-pricing-table table-striped table-hover table'],
        'id' => 'openai-dalle-pricing',
    	
      ],
    ];

    return $build;
  }


}