<?php
namespace Drupal\ai_image_generation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class AIImageAPI extends ConfigFormBase
{

   /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    // Returns the form ID.
    return "ai_image_api_settings";
  }

 /**
   * {@inheritdoc}
   *
   * Returns an array of configuration names that will be editable.
   *
   * @return array
   *
   */
  protected function getEditableConfigNames()
   {  // Returns the names of the configuration objects that this form will edit.

    return ['ai_images_api.settings'] ;
  }


/**
   * {@inheritdoc}
   *
   * Builds the configuration form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    // Define a fieldset to group API settings fields.
    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
    ];

    // Define a textfield for the Organisation ID.
    $form['api_settings']['orgid'] = [
      '#required'=> true,
      '#type'=>'textfield',
      '#title' => $this->t('Organisation ID '),
      '#default_value' => $this->config('ai_images_api.settings')->get('orgid'),
      '#description' => $this->t('This API key is required to interact with OpenAI services. Enter personal or organisation as per yourorganisation type. <br>To get your API key click here and sign up  <a href="@link" target="_blank">OpenAI website</a>.', ['@link' => 'https://openai.com/api']),
     ];

    // Define a textfield for the API Key.
    $form['api_settings']['apikey']=[
      '#required'=> true,
      '#type'=>'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' =>$this->config('ai_images_api.settings')->get('apikey'),
      '#description' => $this->t(' This API key is required to interact with OpenAI services. To get your API key click here <a href="@link" target="_blank">OpenAI website</a>.', ['@link' => 'https://openai.com/api']),
     ];

     return parent::buildForm($form, $form_state);
  }


   /**
   * {@inheritdoc}
   *
   * Form submission handler.
   *
   * @param array &$form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */

  public function submitForm(array &$form, FormStateInterface $form_state)
  { // Save the submitted API Key and Organisation ID to configuration.
    $this->config('ai_images_api.settings')
      ->set('apikey', $form_state->getValue('apikey'))
      ->set('orgid', $form_state->getValue('orgid'))
      ->save();

    // Call the parent submit handler.
    parent::submitForm($form, $form_state);
  }


}


?>