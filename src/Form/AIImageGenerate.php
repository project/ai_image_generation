<?php
namespace Drupal\ai_image_generation\Form;


use Drupal\Core\Url;
use GuzzleHttp\Client;
use Drupal\user\Entity\User;
use Drupal\media\Entity\Media;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;



use Drupal\Component\Serialization\Json;

class AIImageGenerate extends FormBase
{
  protected $checkboxValues;


   /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return "ai_ImageGenerate";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $apikey  =  $this->config('ai_images_api.settings')->get('apikey');
    $apistatus = empty($apikey)? true:false;
    $toalimg =4;
     //==========================  left side setting starts ========================
     $dalle_options = [
      '' => $this->t('Select any'),
      '256x256' => $this->t('256x256'),
      '512x512' => $this->t('512x512'),
      '1024x1024' => $this->t('1024x1024'),
      '1024x1792' => $this->t('1024x1792'),
      '1792x1024' => $this->t('1792x1024'),
    ];
    $form['api_settings_left'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('<span class="ai-tab-heading">Image Generation Forms</span>'),
      '#attributes' => [
        'class' => ['open-ai-image-form'],
      ],
    ];

    // Create textfield  for  image text prompt.
    $form['api_settings_left']['api_element']['imgtext']=[
      '#type'=>'textarea',
      '#title' => $this->t('Enter text for image generations'),
      '#description' => $this->t('Write description of image that you want to generatre through open AI. '),
      '#maxlength' => 255

    ];

    // Create selectbox  for  API model.
    $form['api_settings_left']['api_element']['apimodel']=[
      '#type'=>'select',
      '#title' => $this->t('Select model to generate Image '),
      '#options' => [
        '' => $this->t('Select any model'),
        'dall-e-2' => $this->t('DALL·E 2'),
        'dall-e-3' => $this->t('DALL·E 3'),
      ],
      '#default_value'=>'DALL·E 2',
      '#description' => $this->t('Select API Image Model here. Default will be DALL·E 2'),
      '#id' => 'ai-model',

    ];

    // Create selectbox  for  image size.
    $form['api_settings_left']['api_element']['imgsize']=[
      '#type'=>'select',
      '#title' => $this->t('Select size'),
       '#options' =>$dalle_options,
       '#description' => $this->t('Select Image size here'),
       '#default_value'=>'1024x1024',
      '#prefix' => '<div id="dalle2-imgsize-wrapper" class="dalle-2-img">',
      '#suffix' => '</div>',
      '#id' => 'dalle-imgsize',
    ];

    // Create selectbox  for  image style.
    $d3form['api_settings_left']['api_element']['d3']['d3_style']=[
      '#type'=>'select',
      '#title' => $this->t('Select D3 Image Style '),
      '#options' => [
        'vivid' => $this->t('vivid '),
        'natural' => $this->t('natural'),
      ],
      '#default_value' => 'vivid',
      '#description' => $this->t('Image style parameter is applicable only in Dall-E-3 Model'),
    ];

      // Create selectbox  for  image quality.
    $d3form['api_settings_left']['api_element']['d3']['d3_quality']=[
      '#type'=>'select',
      '#title' => $this->t('Select D3 Image quality '),
      '#options' => [
        'standard' => $this->t('standard'),
        'hd' => $this->t('HD'),
      ],
      '#default_value' => 'standard',
      '#description' => $this->t('Image quality parameter is applicable only in Dall-E-3 Model'),
    ];

    // Create selectbxo  for no. of imaages.
    $form['api_settings_left']['api_element']['imgcount']=[
      '#type'=>'select',
      '#title' => $this->t('No of Image'),
      '#id' => 'aitotalimage',
      '#options' => [
        intval('1') => $this->t('1'),
        intval('2') => $this->t('2'),
        intval('3') => $this->t('3'),
        intval('4') => $this->t('4'),
      //  intval('5') => $this->t('5'),
      ],
      '#default_value' => intval('1'),
      '#description' => $this->t('Dall-e-3 will generate one image only'),
    ];

    // Create container for d3 extra paramneter like   image quality and style
    $form['api_settings_left']['api_element']['d3'] = [
      $d3form,
     '#prefix' => '<div id="d3image-parameter-wrapper" class="d3-parameter hide-this">',
     '#suffix' => '</div>',
   ];

    // Create  "Generate Image" button.
    $form ['api_settings_left']['button'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate Image'),
      '#disabled' => $apistatus,
      '#ajax' => [
        'callback' => '::aiImageGeneration',
        'event' => 'click',
        'wrapper' => 'image-preview',
        'effect' => 'fade',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Generating image...'),
        ],
      ],
	 '#attributes' => [
        'class' => ['customai-btn'],
      ],
    ];

    $form ['api_settings_left']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Image'),
      '#id' => 'aiimagesavebtn',
	  '#attributes' => [
        'class' => ['customai-btn'],
      ],
    ];

    //==========================  Right side setting starts ========================
    $form['api_settings_right'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('<span class="ai-tab-heading ">Image Canvas</span>'),
      '#attributes' => [
        'class' => ['open-ai-image-form'],]
    ];
          // Showing openaI price structure.
    $form['api_settings_right']['canvas']['pricelink'] =  [
      '#type' => 'link',
      '#title' => $this->t('<b>Check OpenAI Pricing Structure  </b>'),
      '#url' => Url::fromRoute('ai_image_generation.usage_page'),
      '#attributes' => [
          'class' => ['use-ajax btn btn-aibutton'],
          'data-dialog-type' => 'dialog',
          'data-dialog-renderer' => 'off_canvas',
          'data-dialog-options' => Json::encode([
            'width' => '27%',
            'height' => '30%',
           ]),
      ],

      '#prefix' => '<div id="aiprice-wrapper">',
      '#suffix' => '</div>',
  ];
   //link to usage
  $form['api_settings_right']['canvas']['pricelink']['additional_markup'] = [
    '#markup' => $this->t("<b><a class='btn btn-aibutton' href='https://platform.openai.com/usage'>Login here to View the AI usage</a></b>"),
    '#attributes' => [
      'class' => ['use-ajax btn btn-aibutton'],
    ],
  ];

  // Create five checkboxes.
    for ($i = 0; $i < $toalimg; $i++) {
      $formsaveimg['aicheckbox_' . $i] = [
        '#type' => 'checkbox',
        '#title' => $this->t('save Image-@number', ['@number' => ($i+1)]),
        '#attributes' => [
          'class' => ['aiimage']
        ],
        '#id' => 'AIimage-checkbox-' . $i,
        '#prefix' => '<div id="checkbox-img-wrapper-' . $i . '" class="hide-this aiimage-save">',
        '#suffix' => '</div>',
      ];
    }

    // Created container for  five checkboxes.
    $form['api_settings_right']['canvas']['save_image'] = [
        $formsaveimg,
        '#prefix' => '<div id="AI-imagesave">',
        '#suffix' => '</div>',
    ];
    $form['api_settings_right']['image_preview_canvas'] ['msg']=  [
      '#type' => 'container',
      '#markup' => '',
       '#prefix' => '<div id="AI-msg-wrapper">',
      '#suffix' => '</div>'
    ];

    // Created container for image.
    $form['api_settings_right']['image_preview_canvas']['img']=  [
      '#type' => 'container',
      '#markup' =>  $this->t(' <h3> AI-Image Canvas...... </h3>'),
      '#attributes' => [
        'id' => 'AI-imageview',
        'class' => 'AI-base-imageview',
      ],
      '#prefix' => '<div id="AI-imageview">',
      '#suffix' => '</div>',
    ];
    return $form;
  }

  public function aiImageGeneration(array &$form, FormStateInterface $form_state) {

    $responseajx = new AjaxResponse();
    \Drupal::service('session')->remove('new_ai_image');
    $imgtext=trim($form_state->getValue('imgtext'));
    if (empty( $imgtext)){
      $formmsg['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t( " <h4 class='ai-errormsg'> No Text Prompt Error:  </h4> <br> ".  " Please describe      the image you’d like me to create, and I’ll do my best to generate it for you."),
        '#prefix' => '<div id="AI-imageview">',
        '#suffix' => '</div>',
      ];
      $responseajx->addCommand(new ReplaceCommand('#AI-imageview',  $formmsg ));
      return $responseajx;
    }
    $imgcount =  empty( $imgcount)?1:$imgcount;
    $imgmodel =  empty( $form_state->getValue('apimodel'))?'dall-e-2':$form_state->getValue('apimodel');
    $imgcount = intval( $form_state->getValue('imgcount'));
    $imagecount = ( $imgmodel==='dall-e-3')?1:$imgcount;
    $imgsize = $form_state->getValue('imgsize');
    $quality=$style='';

    if( $imgmodel=="dall-e-3")
    {
      $quality= empty( $form_state->getValue('d3_quality'))?'standard':$form_state->getValue('d3_quality');
      $style= empty( $form_state->getValue('d3_style'))?'natural': $form_state->getValue('d3_style');
    }
    $response= $this->fetchimagebyAPI ( $imgmodel, $imgtext, $imagecount, $imgsize,$quality, $style ); //api data
    $statusCode = $response->getStatusCode();//api data
    $responcedata = json_decode($response->getBody()->getContents(), TRUE);   //---for api data

    if ($statusCode === 200){
       $baseimg = $responcedata;    //---for api data
       $output= $this->showAiImage($baseimg);
       $formmsg=  [
        '#type' => 'container',
        '#markup' =>  $this->t(' <h2> Here are the image/images...... </h2>'),
        '#prefix' => '<div id="AI-msg-wrapper">',
        '#suffix' => '</div>'
      ];

      \Drupal::service('session')->set('new_ai_image', $baseimg['data']);
      $responseajx->addCommand(new ReplaceCommand('#AI-msg-wrapper',  $formmsg));
      $responseajx->addCommand(new ReplaceCommand('#AI-imageview',  $output['image_element'] ));
      for($i=0;$i<4;$i++)
      {
         $responseajx->addCommand(new InvokeCommand('#AIimage-checkbox-'.$i, 'prop', ['checked', false]));
      }
      return $responseajx;
    }

    if ($statusCode !== 200)
    {
      $errMessage= $responcedata ['error']['message'];
      $errorMessage=( $statusCode===429)?"Image limit exceeded for images per minute. Maxumum 5 images per minute is allowed ": $errMessage;
      $formmsg['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t( "<h4 class='ai-errormsg'> Error :  </h4> <br> ".  $errorMessage),
        '#prefix' => '<div id="AI-imageview">',
        '#suffix' => '</div>',
      ];
      $responseajx->addCommand(new ReplaceCommand('#AI-imageview',  $formmsg ));
      return $responseajx;
    }
  }


public function submitForm(array &$form, FormStateInterface $form_state)
{
    $opensai_image = \Drupal::service('session')->get('new_ai_image');
    \Drupal::logger('AI_Image > Temporary AI Image')->notice(print_r( $opensai_image, 1));
    if (is_null($opensai_image)) {
      $this->messenger()->addError(t("IMAGE SAVE ERROR: The image data is missing or invalid."));
      return;
    }
    $imgcount = intval( $form_state->getValue('imgcount')) ;
    $imgtextmsg= " ";
	$imgflag=0;
     for ($i = 0; $i < $imgcount; $i++) {
        $checkbox_value = $form_state->getValue('aicheckbox_' . $i);
        if ($checkbox_value) {
          $binaryimg = base64_decode( $opensai_image[$i]['b64_json'] );
          $filename =  'ai_image' . ($i + 1) . '_' . uniqid() . '.jpg';

          // Save the file entity.
          $file_path = 'public://' . $filename;
          $file = \Drupal::service('file.repository')->writeData($binaryimg, $file_path, FileSystemInterface::EXISTS_REPLACE);

          // Create a media entity
          if ($file) {
            $media = Media::create([
              'bundle' => 'image',
              'uid' => User::load(\Drupal::currentUser()->id()),
              'field_media_image' => [
                'target_id' => $file->id(),
                'alt' => 'Descriptive text',
              ],
              'name' => $file->getFilename(),
            ]);
            $media->set('status', TRUE);
            $media->save();
			$imgflag=1;
            $imgtextmsg= $imgtextmsg. "  Image".($i + 1);
            $this->messenger()->addMessage(t("<b> AI  image-@no  </b> saved in media library with name - <b> @name</b><br> ",['@no'=>$i+1,'@name'=> $filename]));
          }
          else {
            // Display an error message.
            $this->messenger()->addError(t("IMAGE SAVE ERROR: The image-'@image' data is missing or invalid.",[@image=>($i + 1)]));
          }
        } //if ends
      } //for loop ends

      if( $imgflag==0){
        $this->messenger()->addError(t("IMAGE SAVE ERROR: The image selection is missing or invalid."));
      }
      else {
		$url = Url::fromRoute('entity.media.collection')->toString();
		$this->messenger()->addMessage(t(" <br><b><a href='@url'>Click here to View image in media library </a></b>", ['@url' => $url, ]));
      }
      \Drupal::service('session')->remove('new_ai_image');

  }   // function ends





  /**
   * function to show image
   *
   */
  public function showAiImage( $responcedata){
     $baseimg = $responcedata;
     $count =count($baseimg['data']);
     \Drupal::logger('AI Image > responce image data ')->notice(print_r( $baseimg['data'],1));
     for($i=0; $i<$count; $i++) {
        $aiimage_container[$i] = [
          '#type' => 'container',
          'image' => [
              '#type' => 'html_tag',
              '#tag' => 'img',
              '#attributes' => [
                'src' => 'data:image/jpeg;base64,' . $baseimg['data'][$i]['b64_json'],
                'class' => 'AI-base-image-'.$i,
              ],
           ],
          'save_checkbox'.$i => [
            '#type' => 'checkbox',
            '#title' => 'Save Image ' . ($i + 1),
            '#attributes' => ['id' => 'save-image-checkbox-'.$i],
            '#prefix' => '<div class="aibox">',
            '#suffix' => '</div>',
          ],
          '#attributes' => [
            'id' => 'Image-view-'.$i,
            'class' => 'AIImageview',
          ],
        ];
      }
      $image_wrapper['image_element']  = [
        $aiimage_container,
        '#prefix' => '<div id="AI-imageview">',
        '#suffix' => '</div>',
      ];
      return $image_wrapper;
   }


   /**
 *   function to fetch data by API
 * @param [type] imgmodel
 * @param [type] $imgtext
 * @param [type] $imgcount
 * @param [type] $imgsize
 * @param [type] $quality
 * @param [type] $style
 * @return void
 */
  public function fetchimagebyAPI(  $imgmodel, $imgtext, $imgcount , $imgsize,$quality, $style )
  {
    $apiendpoint  = 'https://api.openai.com/v1/images/generations';
    $apikey  =  $this->config('ai_images_api.settings')->get('apikey');
    $orgidkey = $this->config('ai_images_api.settings')->get('orgid');

    // set the API parameters here
    $client= \Drupal::httpClient();
    $client = new Client();
    $headers =[
      'Authorization' => 'Bearer ' . $apikey,
      'Content-Type' => 'application/json',
     ];
    $body=[
      'model'=> $imgmodel,
      'prompt'=>$imgtext,
      'size'=> $imgsize,
      'n' => $imgcount ,
      'response_format' => 'b64_json',
    ];

    if( $imgmodel=="dall-e-3"   )
    {  $body=[
          'model'=> $imgmodel,
          'prompt'=>$imgtext,
          'size'=> $imgsize,
          'n' => $imgcount ,
          'response_format' => 'b64_json',
          'style'=> empty($style)?'natural':$style,
          'quality'=> empty($quality)?'standard':$quality,
        ];
    }
    try{
      $response = $client->post($apiendpoint, ['headers' =>  $headers, 'json' => $body, ]);

    }catch (\Exception $e) {
        \Drupal::logger('AI Image > General Exception')->error($e->getMessage());
        if ($e->hasResponse()) {
          $response = $e->getResponse();
        }
    }
    \Drupal::logger('AI Image > Request ')->notice(print_r( $body,1));
    \Drupal::logger('AI Image > Response ')->notice(print_r( $response,1));
    \Drupal::logger('AI Image > statuscode')->notice(print_r( $response->getStatusCode(),1));
     return   $response;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }


}

 ?>